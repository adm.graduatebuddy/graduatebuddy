<script>
    (function($) {
        var dataPaket = $('#dataPaket').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('cms/get_data_paket') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0, 4],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });
    })(jQuery);

    function edit_paket(id) {
        $('#form_edit_paket').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_paket') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#paket_id_edit').val(data.id);
                $('#paket_name_edit').val(data.nama_paket);
                $('#description_edit').val(data.keterangan);
                $('#harga_paket_edit').val(data.harga_paket);
                $('#icon_edit').val(data.icon);
                $('#edit_paket').modal('show'); // show bootstrap modal when complete loaded
                // $('.modal-title').text('Edit Menu'); // Set title to Bootstrap modal title
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_paket(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('cms/delete_paket/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataPaket').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }
</script>
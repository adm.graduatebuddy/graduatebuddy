<script type="text/javascript">
    (function($) {
        $.getJSON(
            'paket_data',
            function(result) {
                $('#paket_add').empty();
                $('#paket_add').append('<option value="">Pilih ...</option>');
                $('#paket_edit').empty();
                $('#paket_edit').append('<option value="">Pilih ...</option>');
                $.each(result.result, function() {
                    $('#paket_add').append('<option value="' + this['id'] + '">' + this['nama_paket'] + '</option>');
                    $('#paket_add').prop('disabled', false);
                    $('#paket_edit').append('<option value="' + this['id'] + '">' + this['nama_paket'] + '</option>');
                    $('#paket_edit').prop('disabled', false);
                });
                $('#paket_add').selectpicker("refresh");
                $('#paket_edit').selectpicker("refresh");
            }
        );

        var dataFilePDF = $('#dataFilePDF').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('cms/get_data_pdf') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });
    })(jQuery);

    function show_pdf(id) {
        // $('#form_user_admin').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#file_pdf').empty();
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_file_pdf') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#file_pdf').append("<iframe frameborder='0' width='100%' height='500' src='<?= base_url('pdf/'); ?>" + data.file_name + "/' scrolling='no' marginwidth='0' marginheight='0' allowFullScreen></iframe>");
                $('#show_file_pdf').modal('show'); // show bootstrap modal when complete loaded
                // $('.modal-title').text('View Photo Profile Admin'); // Set title to Bootstrap modal title
            }
        });
    }

    function edit_file_pdf(id) {
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#image_edit').empty();
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_file_pdf') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#file_pdf_id_edit').val(data.id);
                $('#judul_edit').val(data.judul);
                $('#file_pdf_edit').val(data.file_name);
                $('select[name="kategori_edit"]').val(data.kategori);
                $('select[name="paket_edit"]').val(data.id_paket);
                $('select[name="status_id"]').val(data.is_active);
                $('.selectpicker').selectpicker('refresh');
                $('#edit_file_pdf').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_file_pdf(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('cms/delete_file_pdf/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataFilePDF').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }
</script>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--   <meta content="IE=edge" http-equiv="X-UA-Compatible"> -->
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="GRADUATE BUDDY" name="description">
    <meta content="Muhammad Sholihin" name="author">
    <link href="<?= base_url('ico/favicon.ico'); ?>" rel="shortcut icon">
    <?php foreach ($web as $w) : ?>
        <title><?= $w['title']; ?></title>
    <?php endforeach ?>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.css'); ?>">
    <!-- Bootstrap theme -->
    <!--  <link rel="stylesheet" href="css/bootstrap-theme.min.css"> -->

    <!-- Custom styles for this template -->
    <!--     <link rel="stylesheet" id="theme" href="css/theme3.css" /> -->
    <link rel="stylesheet" href="<?= base_url('assets/css/theme-nopadding.css'); ?>">
    <script src="https://kit.fontawesome.com/dddf6906b4.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="<?= base_url('assets/css/dashboard.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/css/dripicon.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/css/typicons.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/css/responsive.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/js/chartist/chartist.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/js/vegas/jquery.vegas.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/js/number-progress-bar/number-pb.css'); ?>" />

    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap-select.min.css'); ?>">

    <link rel="stylesheet" href="<?= base_url('assets/js/timepicker/bootstrap-timepicker.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/js/datepicker/datepicker.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/js/datepicker/clockface.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/css/jquery.tagsinput-revisited.css'); ?>" />

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="<?= base_url('assets/js/ckeditor/ckeditor.js'); ?>" type="text/javascript"></script>
    <!-- pace loader -->
    <script src="<?= base_url('assets/js/pace/pace.js'); ?>"></script>
    <link href="<?= base_url('assets/js/pace/themes/orange/pace-theme-flash.css'); ?>" rel="stylesheet" />

    <style>
        .text {
            width: 250px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .select2 {
            width: 100% !important;
        }

        .form-row-wide .intl-tel-input {
            display: block;
        }
    </style>
</head>

<Body role="document">

    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <!-- TOPNAV -->


    <!-- END OF TOPNAV -->
    <!-- Comtainer -->
    <div class="container-fluid paper-wrap bevel tlbr">
        <div id="paper-top">
            <div class="row">
                <div class="col-sm-3 no-pad">
                    <?php foreach ($web as $w) : ?>
                        <a class="navbar-brand logo-text" href="#"><?= $w['name']; ?></a>
                    <?php endforeach ?>
                </div>
                <div class="col-sm-3 no-pad" style="position: absolute;">
                    <div class="navbar-right">
                        <ul class="noft-btn margin-right">

                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 no-pad">
                    <ul style="margin-top:8px;" class="nav navbar-nav navbar-left list-unstyled list-inline text-gray date-list news-list">
                    </ul>
                </div>

                <div class="col-sm-3 no-pad">
                    <div class="navbar-right">
                        <ul class="nav navbar-nav margin-left">
                            <li class="dropdown notifications-menu">
                                <div class="drop-btn dropdown-toggle bg-white" data-toggle="dropdown" title="Data diri anda">
                                    <i class="fa  icon-user text-navy"></i>
                                </div>
                            </li>
                            <!-- Tasks: style can be found in dropdown.less -->
                            <li data-toggle="tooltip" data-placement="bottom" title="Log Out">
                                <div class="drop-btn dropdown-toggle bg-white" data-toggle="dropdown" title="Keluar dari aplikasi" onclick="logout()">
                                    <i class="fa icon-upload text-navy"></i>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- end of menu right -->
            </div>
        </div>
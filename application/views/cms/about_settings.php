<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="col-12">
        <?= $this->session->flashdata('message'); ?>
    </div>

    <div class="col-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <?php foreach ($atas as $at) : ?>
                    <p style="font-size:15px;"><b>About Menu Setting</b> - <?= $at['judul']; ?></p>
                <?php endforeach; ?>
                <button class="btn-link d-edit" data-toggle="modal" data-target="#edit_atas"><i class="fa fa-edit" style="color:blue; font-size:15px; padding-top:-50px;"></i></button>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

    <div class="col-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <?php foreach ($bawah as $baw) : ?>
                    <p style="font-size:15px;"><b>About Menu Setting</b> - <?= $baw['judul']; ?></p>
                <?php endforeach; ?>
                <button class="btn-link d-edit" data-toggle="modal" data-target="#edit_bawah"><i class="fa fa-edit" style="color:blue; font-size:15px; padding-top:-50px;"></i></button>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

    <div id="edit_atas" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Custom HTML</h4>
                </div>

                <?php echo form_open_multipart('cms/update_about_atas'); ?>
                <div class="modal-body">
                    <?php foreach ($atas as $at) : ?>
                        <div class="form-group">
                            <label for="usr">Title :</label>
                            <input type="text" class="form-control" id="title_atas" name="title_atas" value="<?= $at['judul']; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Copyright Text :</label>
                            <textarea class="form-control" id="content_atas" name="content_atas" style="height: 250px" required><?= $at['content']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="usr">Logo :</label>
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <input type="hidden" id="old_logo" name="old_logo" value="<?= $at['image']; ?>" />
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                            <img src="<?php echo base_url('assets/uploads/single/'); ?><?= $at['image']; ?>" height="250px" style="margin-top: 10px;">
                        </div>

                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="status_id" id="status_id" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0" <?php if ($at['is_active'] == 0) echo "selected"; ?>>Hidden</option>
                                <option value="1" <?php if ($at['is_active'] == 1) echo "selected"; ?>>Show</option>
                            </select>
                        </div>

                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <div id="edit_bawah" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Custom HTML</h4>
                </div>

                <?php echo form_open_multipart('cms/update_about_bawah'); ?>
                <div class="modal-body">
                    <?php foreach ($bawah as $baw) : ?>
                        <div class="form-group">
                            <label for="usr">Title :</label>
                            <input type="text" class="form-control" id="title_bawah" name="title_bawah" value="<?= $baw['judul']; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Copyright Text :</label>
                            <textarea class="form-control" id="content_bawah" name="content_bawah" style="height: 250px" required><?= $baw['content']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="usr">Logo :</label>
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <input type="hidden" id="old_logo" name="old_logo" value="<?= $baw['image']; ?>" />
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                            <img src="<?php echo base_url('assets/uploads/single/'); ?><?= $baw['image']; ?>" height="250px" style="margin-top: 10px;">
                        </div>

                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="status_id" id="status_id" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0" <?php if ($baw['is_active'] == 0) echo "selected"; ?>>Hidden</option>
                                <option value="1" <?php if ($baw['is_active'] == 1) echo "selected"; ?>>Show</option>
                            </select>
                        </div>

                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->
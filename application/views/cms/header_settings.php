<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="col-12">
        <?= $this->session->flashdata('message'); ?>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red">Logo Website</span>
                        </h2>
                    </div>
                    <?php foreach ($logo as $l) : ?>
                        <p style="font-size:15px;"><b>Logo Settings</b> - <?= $l['judul']; ?></p>
                        <img src="<?php echo base_url('assets/uploads/logo/'); ?><?= $l['image']; ?>" height="70px"><br /><br />
                        <button style="text-align: right;" class="btn-link d-edit" data-toggle="modal" data-target="#edit_kiri" title="Edit"><i class="fa fa-edit" style="color:blue; font-size:15px; padding-top:-50px;"></i></button>
                    <?php endforeach; ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-lg-8">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red">Menu Header</span>
                        </h2>
                    </div>
                    <div class="box-body table-responsive">
                        <?= $this->session->flashdata('message-menu'); ?>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#menuSetting">Tambah Menu</button>
                        <br /><br />
                        <table id="dataMenuSetting" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th>Menu</th>
                                    <th>Link</th>
                                    <th style="width: 18%;">Status Login</th>
                                    <th style="width: 5%;">Status</th>
                                    <th style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
        </div>

        <div class="col-lg-8">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red">Submenu Header</span>
                        </h2>
                    </div>
                    <div class="box-body table-responsive">
                        <?= $this->session->flashdata('message-submenu'); ?>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#submenuSetting">Tambah Submenu</button>
                        <br /><br />
                        <table id="dataSubMenuSetting" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th>Menu</th>
                                    <th>Submenu</th>
                                    <th>Link</th>
                                    <th style="width: 18%;">Status Login</th>
                                    <th style="width: 5%;">Status</th>
                                    <th style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="edit_kiri" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Logo Settings</h4>
                    </div>

                    <?php echo form_open_multipart('cms/add_logo_header'); ?>
                    <div class="modal-body">
                        <?php foreach ($logo as $l) : ?>
                            <div class="form-group">
                                <label for="usr">Title :</label>
                                <input type="text" class="form-control" id="title-header" name="title-header" value="<?= $l['judul']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="usr">Link :</label>
                                <input type="text" class="form-control" id="content-header" name="content-header" value="<?= $l['content']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="usr">Logo :</label>
                                <input type="file" class="form-control-file" id="logo" name="logo"></input>
                                <input type="hidden" id="old_logo" name="old_logo" value="<?= $l['image']; ?>" />
                                <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                                <img src="<?php echo base_url('assets/uploads/logo/'); ?><?= $l['image']; ?>" height="100px" style="margin-top: 10px;">
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="menuSetting" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Menu</h4>
                    </div>

                    <?php echo form_open_multipart('cms/add_menu_setting'); ?>
                    <div class="modal-body">
                        <?php foreach ($logo as $l) : ?>
                            <div class="form-group">
                                <label for="usr">Menu :</label>
                                <input type="text" class="form-control" id="menu_name_add" name="menu_name_add">
                            </div>
                            <div class="form-group">
                                <label for="usr">Link :</label>
                                <input type="text" class="form-control" id="menu_link_add" name="menu_link_add">
                            </div>
                            <div class="form-group">
                                <label for="usr">Status Login :</label>
                                <select name="status_login_add" id="status_login_add" class="form-control" required>
                                    <option value="">Pilih Status</option>
                                    <option value="login">Login</option>
                                    <option value="not_login">Not Login</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="usr">Status :</label>
                                <select name="menu_status_id" id="menu_status_id" class="form-control" required>
                                    <option value="">Pilih Status</option>
                                    <option value="0">Non Active</option>
                                    <option value="1">Active</option>
                                </select>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="edit-menu-setting" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Menu</h4>
                    </div>

                    <form action="<?= base_url('cms/update_menu_setting'); ?>" method="post" id="form_edit_menu">
                        <div class="modal-body">
                            <?php foreach ($logo as $l) : ?>
                                <div class="form-group">
                                    <label for="usr">Menu :</label>
                                    <input type="hidden" class="form-control" id="menu_id_edit" name="menu_id_edit">
                                    <input type="text" class="form-control" id="menu_name_edit" name="menu_name_edit">
                                </div>
                                <div class="form-group">
                                    <label for="usr">Link :</label>
                                    <input type="text" class="form-control" id="menu_link_edit" name="menu_link_edit">
                                </div>
                                <div class="form-group">
                                    <label for="usr">Status Login :</label>
                                    <select name="status_login_edit" id="status_login_edit" class="form-control" required>
                                        <option value="">Pilih Status</option>
                                        <option value="login">Login</option>
                                        <option value="not_login">Not Login</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Status :</label>
                                    <select name="menu_status_id" id="menu_status_id" class="form-control" required>
                                        <option value="">Pilih Status</option>
                                        <option value="0">Non Active</option>
                                        <option value="1">Active</option>
                                    </select>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="submenuSetting" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Submenu</h4>
                    </div>

                    <form action="<?= base_url('cms/add_submenu_setting'); ?>" method="post">
                        <div class="modal-body">
                            <?php foreach ($logo as $l) : ?>
                                <div class="form-group">
                                    <label for="usr">Menu :</label>
                                    <select name="submenu_menu_id_add" id="submenu_menu_id_add" class="form-control" required>
                                        <option value="">Pilih Nama Menu</option>
                                        <?php foreach ($menu as $m) : ?>
                                            <option value="<?= $m['id']; ?>"><?= $m['menu_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Submenu :</label>
                                    <input type="text" class="form-control" id="submenu_name_add" name="submenu_name_add">
                                </div>
                                <div class="form-group">
                                    <label for="usr">Link :</label>
                                    <input type="text" class="form-control" id="submenu_link_add" name="submenu_link_add">
                                </div>
                                <div class="form-group">
                                    <label for="usr">Status Login :</label>
                                    <select name="sub_status_login_add" id="sub_status_login_add" class="form-control" required>
                                        <option value="">Pilih Status</option>
                                        <option value="login">Login</option>
                                        <option value="not_login">Not Login</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Status :</label>
                                    <select name="submenu_status_id" id="submenu_status_id" class="form-control" required>
                                        <option value="">Pilih Status</option>
                                        <option value="0">Non Active</option>
                                        <option value="1">Active</option>
                                    </select>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="edit-submenu-setting" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Submenu</h4>
                    </div>

                    <form action="<?= base_url('cms/update_submenu_setting'); ?>" method="post" id="form_edit_submenu">
                        <div class="modal-body">
                            <?php foreach ($logo as $l) : ?>
                                <div class="form-group">
                                    <label for="usr">Menu :</label>
                                    <input type="hidden" class="form-control" id="submenu_id_edit" name="submenu_id_edit">
                                    <select name="submenu_menu_id_edit" id="submenu_menu_id_edit" class="form-control" required>
                                        <option value="">Pilih Nama Menu</option>
                                        <?php foreach ($menu as $m) : ?>
                                            <option value="<?= $m['id']; ?>"><?= $m['menu_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Submenu :</label>
                                    <input type="text" class="form-control" id="submenu_name_edit" name="submenu_name_edit">
                                </div>
                                <div class="form-group">
                                    <label for="usr">Link :</label>
                                    <input type="text" class="form-control" id="submenu_link_edit" name="submenu_link_edit">
                                </div>
                                <div class="form-group">
                                    <label for="usr">Status Login :</label>
                                    <select name="sub_status_login_edit" id="sub_status_login_edit" class="form-control" required>
                                        <option value="">Pilih Status</option>
                                        <option value="login">Login</option>
                                        <option value="not_login">Not Login</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Status :</label>
                                    <select name="submenu_status_id_edit" id="submenu_status_id_edit" class="form-control" required>
                                        <option value="">Pilih Status</option>
                                        <option value="0">Non Active</option>
                                        <option value="1">Active</option>
                                    </select>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/paper bg -->
</div>
<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">

                        <span class="box-btn" data-widget="collapse"><i class="fa fa-minus"></i>
                        </span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span>Data <?php echo $menu_title; ?></span>
                    </h3>
                </div>
                <div class="box-body table-responsive">
                    <ul class="nav nav-tabs">
                        <li <?php if ($tab == "post_blog" || $tab == null) {
                                echo "class='active'";
                            } ?>><a data-toggle="tab" href="#post_blog">Postingan Blog</a></li>
                        <li <?php if ($tab == "post_lowongan") {
                                echo "class='active'";
                            } ?>><a data-toggle="tab" href="#post_lowongan">Postingan Lowongan</a></li>
                        <li <?php if ($tab == "comment_post") {
                                echo "class='active'";
                            } ?>><a data-toggle="tab" href="#comment_post">Komentar</a></li>
                    </ul>
                </div>
                <!-- /.box-header -->
                <div class="tab-content">
                    <div id="post_blog" class="tab-pane fade <?php if ($tab == "post_blog" || $tab == null) {
                                                                    echo "in active";
                                                                } ?>">
                        <div class="box-body table-responsive">
                            <?= $this->session->flashdata('message'); ?>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_blog">Tambah Postingan Blog</button>
                            <br /><br />
                            <table id="dataBlog" class="table table-bordered table-striped" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">#</th>
                                        <th>Judul</th>
                                        <th>Konten</th>
                                        <th>Image</th>
                                        <th style="width: 10%;">Creator</th>
                                        <th style="width: 10%;">Tag</th>
                                        <th style="width: 10%;">Kategori</th>
                                        <th style="width: 5%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <div id="post_lowongan" class="tab-pane fade <?php if ($tab == "post_lowongan") {
                                                                        echo "in active";
                                                                    } ?>">
                        <div class="box-body table-responsive">
                            <?= $this->session->flashdata('message'); ?>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_lowongan">Tambah Postingan Lowongan</button>
                            <br /><br />
                            <table id="dataLowongan" class="table table-bordered table-striped" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">#</th>
                                        <th>Judul</th>
                                        <th>Konten</th>
                                        <th>Image</th>
                                        <th style="width: 10%;">Creator</th>
                                        <th style="width: 10%;">Tag</th>
                                        <th style="width: 10%;">Kategori</th>
                                        <th style="width: 5%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <div id="comment_post" class="tab-pane fade <?php if ($tab == "comment_post" || $tab == null) {
                                                                    echo "in active";
                                                                } ?>">
                        <div class="box-body table-responsive">
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

        <!-- Modal -->
        <div id="add_blog" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Posting Blog</h4>
                    </div>
                    <?php echo form_open_multipart('cms/add_blog'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Judul :</label>
                            <input type="text" class="form-control" id="judul_add" name="judul_add" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Konten :</label>
                            <textarea class="form-control" id="content_add" name="content_add" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="usr">Image :</label>
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                        </div>
                        <div class="form-group">
                            <label for="usr">Tag :</label>
                            <input type="text" class="form-control" id="tag_add" name="tag_add" required>
                        </div>
                        <div class="form-group" style="padding-bottom: 100px;">
                            <label for="usr">Kategori :</label>
                            <select name="category_add" id="category_add" class="selectpicker" data-live-search="true" data-width="100%" required>
                                <option value="">Pilih ...</option>
                            </select>
                            <a href="#" onclick="return addKategoriBlog('add')">
                                <font style="font-size: 10px;"><b>Tambah Kategori +</b></font>
                            </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="show_profile_admin" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">View <?php echo $menu_title; ?></h4>
                    </div>
                    <div style="text-align: center; margin-top:50px; margin-bottom:50px;">
                        <span id="photo_admin"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                    </>
                </div>
            </div>
        </div>

        <div id="edit_blog" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Blog</h4>
                    </div>
                    <?php echo form_open_multipart('cms/edit_blog'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Judul :</label>
                            <input type="hidden" class="form-control" id="blog_edit_id" name="blog_edit_id" required>
                            <input type="text" class="form-control" id="judul_edit" name="judul_edit" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Konten :</label>
                            <textarea class="form-control" id="content_edit" name="content_edit" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="usr">Image :</label>
                            <input type="hidden" id="old_logo" name="old_logo" />
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                            <span id="image_edit"></span>
                        </div>
                        <div class="form-group">
                            <label for="usr">Tag :</label>
                            <input type="text" class="form-control" id="tag_edit" name="tag_edit" required>
                        </div>
                        <div class="form-group" style="padding-bottom: 100px;">
                            <label for="usr">Kategori :</label>
                            <select name="category_edit" id="category_edit" class="selectpicker" data-live-search="true" data-max-options="3" data-width="100%" required>
                                <option value="">Pilih ...</option>
                            </select>
                            <a href="#" onclick="return addKategoriBlog('edit')">
                                <font style="font-size: 10px;"><b>Tambah Kategori +</b></font>
                            </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="add_lowongan" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Posting Lowongan</h4>
                    </div>
                    <?php echo form_open_multipart('cms/add_lowongan'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Judul :</label>
                            <input type="text" class="form-control" id="lowongan_judul_add" name="lowongan_judul_add" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Konten :</label>
                            <textarea class="form-control" id="lowongan_content_add" name="lowongan_content_add" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="usr">Image :</label>
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                        </div>
                        <div class="form-group">
                            <label for="usr">Tag :</label>
                            <input type="text" class="form-control" id="lowongan_tag_add" name="lowongan_tag_add" required>
                        </div>
                        <div class="form-group" style="padding-bottom: 100px;">
                            <label for="usr">Kategori :</label>
                            <select name="lowongan_category_add" id="lowongan_category_add" class="selectpicker" data-live-search="true" data-width="100%" required>
                                <option value="">Pilih ...</option>
                            </select>
                            <a href="#" onclick="return addKategoriLowongan('add')">
                                <font style="font-size: 10px;"><b>Tambah Kategori +</b></font>
                            </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="show_profile_admin" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">View <?php echo $menu_title; ?></h4>
                    </div>
                    <div style="text-align: center; margin-top:50px; margin-bottom:50px;">
                        <span id="photo_admin"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                    </>
                </div>
            </div>
        </div>

        <div id="edit_lowongan" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Lowongan</h4>
                    </div>
                    <?php echo form_open_multipart('cms/edit_lowongan'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Judul :</label>
                            <input type="hidden" class="form-control" id="lowongan_edit_id" name="lowongan_edit_id" required>
                            <input type="text" class="form-control" id="lowongan_judul_edit" name="lowongan_judul_edit" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Konten :</label>
                            <textarea class="form-control" id="lowongan_content_edit" name="lowongan_content_edit" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="usr">Image :</label>
                            <input type="hidden" id="lowongan_old_logo" name="lowongan_old_logo" />
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                            <span id="lowongan_image_edit"></span>
                        </div>
                        <div class="form-group">
                            <label for="usr">Tag :</label>
                            <input type="text" class="form-control" id="lowongan_tag_edit" name="lowongan_tag_edit" required>
                        </div>
                        <div class="form-group" style="padding-bottom: 100px;">
                            <label for="usr">Kategori :</label>
                            <select name="lowongan_category_edit" id="lowongan_category_edit" class="selectpicker" data-live-search="true" data-max-options="3" data-width="100%" required>
                                <option value="">Pilih ...</option>
                            </select>
                            <a href="#" onclick="return addKategoriLowongan('edit')">
                                <font style="font-size: 10px;"><b>Tambah Kategori +</b></font>
                            </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->
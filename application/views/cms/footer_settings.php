<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="col-12">
        <?= $this->session->flashdata('message'); ?>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red">Footer Kiri</span>
                        </h2>
                    </div>
                    <p style="font-size:15px;"><b>Custom HTML</b></p>
                    <button class="btn-link d-edit" data-toggle="modal" data-target="#edit_kiri" title="Edit"><i class="fa fa-edit" style="color:blue; font-size:15px; padding-top:-50px;"></i></button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-lg-4">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red">Footer Tengah</span>
                        </h2>
                    </div>
                    <?php foreach ($tengah as $teng) : ?>
                        <p style="font-size:15px;"><b>Custom HTML</b> <?php if ($teng['judul'] != null) {
                                                                            echo "- " . $teng['judul'];
                                                                        } ?> </p>
                    <?php endforeach; ?>
                    <button class="btn-link d-edit" data-toggle="modal" data-target="#edit_tengah" title="Edit"><i class="fa fa-edit" style="color:blue; font-size:15px; padding-top:-50px;"></i></button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-lg-4">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red">Footer Kanan</span>
                        </h2>
                    </div>
                    <?php foreach ($kanan as $kan) : ?>
                        <p style="font-size:15px;"><b>Custom HTML</b> <?php if ($kan['judul'] != null) {
                                                                            echo "- " . $kan['judul'];
                                                                        } ?> </p>
                        </p>
                    <?php endforeach; ?>
                    <button class="btn-link d-edit" data-toggle="modal" data-target="#edit_kanan" title="Edit"><i class="fa fa-edit" style="color:blue; font-size:15px; padding-top:-50px;"></i></button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <!-- Modal -->
        <div id="edit_kiri" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Custom HTML</h4>
                    </div>

                    <?php echo form_open_multipart('cms/update_footer_kiri'); ?>
                    <div class="modal-body">
                        <?php foreach ($kiri as $kir) : ?>
                            <div class="form-group">
                                <label for="usr">Title :</label>
                                <input type="text" class="form-control" id="title-kiri" name="title-kiri" value="<?= $kir['judul']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="usr">Logo :</label>
                                <input type="file" class="form-control-file" id="logo" name="logo"></input>
                                <input type="hidden" id="old_logo" name="old_logo" value="<?= $kir['image']; ?>" />
                                <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                                <img src="<?php echo base_url('assets/uploads/logo/'); ?><?= $kir['image']; ?>" height="100px" style="margin-top: 10px;">
                            </div>
                            <div class="form-group">
                                <label for="usr">Content :</label>
                                <textarea class="form-control" id="content-kiri" name="content-kiri" style="height: 250px"><?= $kir['content']; ?></textarea>
                            </div>

                        <?php endforeach; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="edit_tengah" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Custom HTML</h4>
                    </div>

                    <?php echo form_open_multipart('cms/update_footer_tengah'); ?>
                    <div class="modal-body">
                        <?php foreach ($tengah as $teng) : ?>
                            <div class="form-group">
                                <label for="usr">Title :</label>
                                <input type="text" class="form-control" id="title-tengah" name="title-tengah" value="<?= $teng['judul']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="usr">Content :</label>
                                <textarea class="form-control" id="content-tengah" name="content-tengah" style="height: 250px"><?= $teng['content']; ?></textarea>
                            </div>

                        <?php endforeach; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="edit_kanan" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Custom HTML</h4>
                    </div>

                    <?php echo form_open_multipart('cms/update_footer_kanan'); ?>
                    <div class="modal-body">
                        <?php foreach ($kanan as $kan) : ?>
                            <div class="form-group">
                                <label for="usr">Title :</label>
                                <input type="text" class="form-control" id="title-tengah" name="title-kanan" value="<?= $kan['judul']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="usr">Content :</label>
                                <textarea class="form-control" id="content-tengah" name="content-kanan" style="height: 250px"><?= $kan['content']; ?></textarea>
                            </div>

                        <?php endforeach; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="edit_bawah" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Custom HTML</h4>
                    </div>

                    <?php echo form_open_multipart('cms/update_footer_bawah'); ?>
                    <div class="modal-body">
                        <?php foreach ($bawah as $baw) : ?>
                            <div class="form-group">
                                <label for="usr">Title :</label>
                                <input type="text" class="form-control" id="title-tengah" name="title-bawah" value="<?= $baw['judul']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="usr">Copyright Text :</label>
                                <textarea class="form-control" id="content-tengah" name="content-bawah" style="height: 250px"><?= $baw['content']; ?></textarea>
                            </div>

                        <?php endforeach; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

    </div>

    <div class="col-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <p style="font-size:15px;"><b>Footer Settings</b> - Copyright Text</p>
                <button class="btn-link d-edit" data-toggle="modal" data-target="#edit_bawah"><i class="fa fa-edit" style="color:blue; font-size:15px; padding-top:-50px;"></i></button>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->
<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="fa fa-minus"></i>
                        </span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span>Data <?php echo $menu_title; ?></span>
                    </h3>
                </div>
                <div class="box-body table-responsive">
                    <ul class="nav nav-tabs">
                        <li <?php if ($tab == "admin" || $tab == null) {
                                echo "class='active'";
                            } ?>><a data-toggle="tab" href="#admin">Data User Admin</a></li>
                        <li <?php if ($tab == "member") {
                                echo "class='active'";
                            } ?>><a data-toggle="tab" href="#member"><span class="label label-warning label-drop" <?php if ($count == '0') {
                                                                                                                        echo "style=\"display:none;\"";
                                                                                                                    } ?>><?= $count; ?></span>Data Member</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="admin" class="tab-pane fade <?php if ($tab == "admin" || $tab == null) {
                                                                    echo "in active";
                                                                } ?>">
                            <br />
                            <?= $this->session->flashdata('message'); ?>
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Tambah User Web</button>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <table id="dataUserAdmin" class="table table-bordered table-striped" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Photo</th>
                                                <th>Role</th>
                                                <th>Status</th>
                                                <th style="width: 11%;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div id="member" class="tab-pane fade <?php if ($tab == "member") {
                                                                    echo "in active";
                                                                } ?>">
                            <br />
                            <?= $this->session->flashdata('message'); ?>
                            <div class="row"></div>
                            <br />
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="dataMember" class="table table-bordered table-striped" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Photo</th>
                                                <th>Paket</th>
                                                <th>Status Paket</th>
                                                <th>Status User</th>
                                                <th style="width: 15%;">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/paper bg -->
        </div>
        <!-- ./wrap-sidebar-content -->
        <!-- / END OF CONTENT -->
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $menu_title; ?></h4>
                </div>
                <form action="<?= base_url('cms/add_user_admin'); ?>" method="post" id="form_user_admin">
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">First Name :</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" required>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Last Name :</label>
                                <input type="text" class="form-control" id="last_name" name="last_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="usr">Username :</label>
                            <input type="text" class="form-control" id="username" name="username" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Email :</label>
                            <input type="text" class="form-control" id="email" name="email" required>
                        </div>
                        <label for="usr">Password :</label>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="password" class="form-control" id="password1" name="password1" required>
                            </div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="password2" name="password2" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="usr">Role :</label>
                            <select name="role_id" id="role_id" class="form-control" required>
                                <option value="">Pilih Nama Role</option>
                                <?php foreach ($role as $r) : ?>
                                    <option value="<?= $r['id']; ?>"><?= $r['role']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="status_id" id="status_id" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                                <option value="2">Block</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="show_profile_admin" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">View <?php echo $menu_title; ?></h4>
                </div>
                <div style="text-align: center; margin-top:50px; margin-bottom:50px;">
                    <span id="photo_admin"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
                </>
            </div>
        </div>
    </div>
    <!-- View User Management Admin -->
    <div id="view_user_admin" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">View <?php echo $menu_title; ?></h4>
                </div>
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="width: 50%"><b>ID</b></td>
                            <td style="width: 50%"><span id="view_id"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Nama</b></td>
                            <td style="width: 50%"><span id="view_name"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Username</b></td>
                            <td style="width: 50%"><span id="view_username"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Email</b></td>
                            <td style="width: 50%"><span id="view_email"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Photo</b></td>
                            <td style="width: 50%"><span id="view_image"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Role</b></td>
                            <td style="width: 50%"><span id="view_role"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Status</b></td>
                            <td style="width: 50%"><span id="view_status"></span></td>
                        </tr>
                    </tbody>
                </table>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div id="edit_user_admin" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $menu_title; ?></h4>
                </div>
                <form action="<?= base_url('cms/edit_users_admin'); ?>" method="post" id="form_edit_user_admin">
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">First Name :</label>
                                <input type="hidden" class="form-control" id="edit_id" name="edit_id" required>
                                <input type="text" class="form-control" id="edit_first_name" name="edit_first_name" required>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Last Name :</label>
                                <input type="text" class="form-control" id="edit_last_name" name="edit_last_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="usr">Username :</label>
                            <input type="text" class="form-control" id="edit_username" name="edit_username" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Email :</label>
                            <input type="text" class="form-control" id="edit_email" name="edit_email" required>
                        </div>
                        <label for="usr">Password :</label>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="password" class="form-control" id="edit_password1" name="edit_password1">
                            </div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="edit_password2" name="edit_password2">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="usr">Role :</label>
                            <select name="edit_role_id" id="edit_role_id" class="form-control" required>
                                <option value="">Pilih Nama Role</option>
                                <?php foreach ($role as $r) : ?>
                                    <option value="<?= $r['id']; ?>"><?= $r['role']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="edit_status_id" id="edit_status_id" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                                <option value="2">Block</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="edit_member" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Member</h4>
                </div>
                <form action="<?= base_url('cms/edit_member'); ?>" method="post" id="form_edit_member">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Nama Lengkap :</label>
                            <input type="hidden" class="form-control" id="member_edit_id" name="member_edit_id" required>
                            <input type="text" class="form-control" id="edit_full_name" name="edit_full_name" required readonly>
                        </div>
                        <div class="form-group">
                            <label for="usr">Email :</label>
                            <input type="text" class="form-control" id="edit_email_member" name="edit_email_member" required readonly>
                        </div>
                        <div class="form-group">
                            <label for="usr">Phone :</label>
                            <input type="text" class="form-control" id="edit_phone_member" name="edit_phone_member" required readonly>
                        </div>
                        <label for="usr">Photo :</label>
                        <div class="form-group">
                            <span id="edit_photo_member"></span>
                        </div>
                        <div class="form-group">
                            <label for="usr">Umur :</label>
                            <input type="text" class="form-control" id="edit_umur" name="edit_umur" required readonly>
                        </div>
                        <div class="form-group">
                            <label for="usr">Kota Domisili :</label>
                            <input type="text" class="form-control" id="edit_domisili" name="edit_domisili" required readonly>
                        </div>
                        <div class="form-group">
                            <label for="usr">Universitas :</label>
                            <input type="text" class="form-control" id="edit_universitas" name="edit_universitas" required readonly>
                        </div>
                        <div class="form-group">
                            <label for="usr">Paket :</label>
                            <select name="edit_paket_id" id="edit_paket_id" class="form-control" required>
                                <option value="">Pilih Nama Paket</option>
                                <?php foreach ($paket as $p) : ?>
                                    <option value="<?= $p['id']; ?>"><?= $p['nama_paket']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="usr">Status Paket :</label>
                            <select name="edit_status_paket_id" id="edit_status_paket_id" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">INACTIVE</option>
                                <option value="1">PENDING</option>
                                <option value="2">ACTIVE</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="edit_status_id" id="edit_status_id" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                                <option value="2">Block</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="view_member" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">View Member</h4>
                </div>
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="width: 50%"><b>ID</b></td>
                            <td style="width: 50%"><span id="view_id_member"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Nama</b></td>
                            <td style="width: 50%"><span id="view_name_member"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Email</b></td>
                            <td style="width: 50%"><span id="view_email_member"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Phone</b></td>
                            <td style="width: 50%"><span id="view_phone_member"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Photo</b></td>
                            <td style="width: 50%"><span id="view_photo_member"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Umur</b></td>
                            <td style="width: 50%"><span id="view_umur_member"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Kota Domisili</b></td>
                            <td style="width: 50%"><span id="view_kota_domisili"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Universitas</b></td>
                            <td style="width: 50%"><span id="view_universitas_member"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Paket</b></td>
                            <td style="width: 50%"><span id="view_paket_member"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Status Paket</b></td>
                            <td style="width: 50%"><span id="view_status_paket_member"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Status</b></td>
                            <td style="width: 50%"><span id="view_status_member"></span></td>
                        </tr>
                    </tbody>
                </table>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <div id="show_profile_member" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">View Profile</h4>
                </div>
                <div style="text-align: center; margin-top:50px; margin-bottom:50px;">
                    <span id="photo_member"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
                </>
            </div>
        </div>
    </div>
</div>
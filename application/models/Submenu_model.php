<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Submenu_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    // start datatables
    var $table = 'cms_submenu_settings';
    var $column_order = array(null, 'b.name_menu', 'a.name_sub', 'a.link', 'a.is_active');
    var $column_search = array('b.menu_name', 'a.name_sub', 'a.link', 'a.is_active'); //set column field database for datatable searchable
    var $order = array('a.id' => 'desc'); // default order

    private function _get_datatables_query()
    {
        $this->db->select('a.*, b.menu_name');
        $this->db->from('cms_submenu_settings as a');
        $this->db->join('cms_menu_settings as b', 'b.id = a.menu_id');

        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_by_id($id)
    {
        $this->db->select('a.*, b.menu_name');
        $this->db->from('cms_submenu_settings as a');
        $this->db->join('cms_menu_settings as b', 'b.id = a.menu_id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    function edit_submenu($id, $menu_id, $submenu, $url, $icon, $is_active)
    {
        $query = "UPDATE `cms_user_sub_menu` SET
                    `menu_id`='$menu_id', 
                    `title`='$submenu', 
                    `url`='$url', 
                    `icon`='$icon', 
                    `is_active`='$is_active'
                  WHERE `id`='$id'";
        $hasil = $this->db->query($query);
        return $hasil;
    }

    function delete_submenu($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function delete_submenu_setting($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("cms_submenu_settings");
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Frontend_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    function view_data($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('cms_settings');
        return $query->result_array();
    }

    function view_menu($flag)
    {
        $this->db->select('*');
        $this->db->where(array('is_active' => '1', 'flag' => $flag));
        $query = $this->db->get('cms_menu_settings');
        return $query->result_array();
    }

    function view_submenu()
    {
        $this->db->select('*');
        $this->db->where('is_active', '1');
        $query = $this->db->get('cms_submenu_settings');
        return $query->result_array();
    }

    function register_member($data)
    {
        $result = $this->db->insert('cms_user_student', $data);
        return $result;
    }

    function view_paket()
    {
        $this->db->select('*');
        $query = $this->db->get('cms_paket_settings');
        return $query->result_array();
    }

    function check_users($email, $phone)
    {
        $this->db->select('*');
        $this->db->from('cms_user_student');
        $where = "email = '$email' AND phone = '$phone'";
        $this->db->where($where);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_profil_member($id)
    {
        $this->db->select('a.id, a.name, a.email, a.phone, a.photo, a.umur, a.universitas_name, a.kota_domisili, a.paket, a.status_paket, a.is_active');
        $this->db->from('cms_user_student as a');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function get_profil($id)
    {
        $this->db->select('a.name, a.email, a.phone, a.photo, a.umur, a.universitas_name, a.kota_domisili, a.paket, a.status_paket, a.is_active');
        $this->db->from('cms_user_student as a');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_data_profil($id)
    {
        $this->db->select('a.name, a.email, a.phone, a.photo, a.umur, a.universitas_name, a.kota_domisili, a.paket, a.status_paket, a.is_active, b.nama_paket');
        $this->db->from('cms_user_student as a');
        $this->db->join('cms_paket_settings as b', 'b.id = a.paket');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_cara_bayar()
    {
        $this->db->select('*');
        $this->db->from('cms_settings as a');
        $this->db->where('id', "9");
        $query = $this->db->get();
        return $query->row();
    }

    function verify_paket($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function get_testimoni()
    {
        $this->db->select('*');
        $this->db->from('cms_testimonial_settings');
        $this->db->where('is_active', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    function config_email_konfirmasi()
    {
        $this->db->select('*');
        $this->db->from('cms_settings');
        $this->db->where('id', 10);
        $query = $this->db->get();
        return $query->row();
    }

    function get_data_paket($id)
    {
        $this->db->select('*');
        $this->db->from('cms_paket_settings');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function get_data_user($id)
    {
        $this->db->select('*');
        $this->db->from('cms_user_student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function config_email_bayar()
    {
        $this->db->select('*');
        $this->db->from('cms_settings');
        $this->db->where('id', 11);
        $query = $this->db->get();
        return $query->row();
    }
}

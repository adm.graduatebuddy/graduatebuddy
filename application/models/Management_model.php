<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Management_model extends CI_Model
{
    function config_data()
    {
        return $this->db->get('cms_management');
    }

    function view_data()
    {
        return $this->db->get('cms_user');
    }

    function get_data()
    {
        return $this->db->get('cms_management')->result_array();
    }

    function get_role()
    {
        $this->db->select('*');
        $this->db->from('cms_user_role');
        $this->db->where('id !=', 1, false);
        return $this->db->get();
    }

    function edit_users($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function delete_users($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function edit_member($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }


    function kabkota_data()
    {
        $query = "SELECT * FROM `cms_tbl_kabkota` WHERE `id` IN('3211', '3212', '5203')";
        return $this->db->query($query)->result_array();
    }

    function korwil_data()
    {
        $query = "SELECT `a`.`id`, `b`.`nm_kabkota`, `a`.`kabkota_id`, `a`.`cd_korwil`, `a`.`nm_korwil` FROM `cms_korwil` AS `a` JOIN `cms_tbl_kabkota` AS `b` ON `b`.`id` = `a`.`kabkota_id`";
        return $this->db->query($query)->result_array();
    }

    function edit_korwil($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function delete_korwil($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}

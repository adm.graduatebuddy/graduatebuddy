<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lowongan_model extends CI_Model
{
    function fetchAllArticle($limit, $start)
    {
        $this->db->limit($limit, $start);
        $query = $this->db->get_where('cms_blog_post', array('flag' => 'lowongan'), $limit, $start);
        $rows = $query->result();

        if ($query->num_rows() > 0) {
            foreach ($rows as $row) {
                $data[] = $row;
            }
            return $data;
        } else {
            return $data = [];
        }

        return false;
    }

    function fetchTagArticle($limit, $start, $parameter)
    {
        $this->db->limit($limit, $start);
        $this->db->from('cms_blog_post');
        $this->db->like('post_tag', $parameter);
        $query = $this->db->get();
        $rows = $query->result();

        if ($query->num_rows() > 0) {
            foreach ($rows as $row) {
                $data[] = $row;
            }
            return $data;
        } else {
            return $data = [];
        }

        return false;
    }

    function fetchSearchArticle($limit, $start, $search)
    {
        $this->db->limit($limit, $start);
        $this->db->from('cms_blog_post');
        $this->db->like('title', $search, 'both');
        $this->db->like('flag', 'lowongan', 'both');
        $query = $this->db->get();
        $rows = $query->result();

        if ($query->num_rows() > 0) {
            foreach ($rows as $row) {
                $data[] = $row;
            }
            return $data;
        } else {
            return $data = [];
        }

        return false;
    }

    function fetchCategoryArticle($limit, $start, $parameter)
    {
        $this->db->limit($limit, $start);
        $this->db->select('a.*, b.category_name');
        $this->db->from('cms_blog_post a');
        $this->db->join('cms_blog_category b', 'b.id = a.id_category');
        $this->db->like('b.category_name', $parameter);
        $query = $this->db->get();
        $rows = $query->result();

        if ($query->num_rows() > 0) {
            foreach ($rows as $row) {
                $data[] = $row;
            }
            return $data;
        } else {
            return $data = [];
        }

        return false;
    }

    public function get_total()
    {
        return $this->db->count_all('cms_blog_post');
    }

    public function get_total_tag($parameter)
    {
        $this->db->from('cms_blog_post');
        $this->db->like('post_tag', $parameter);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_total_category($parameter)
    {
        $this->db->from('cms_blog_post a');
        $this->db->join('cms_blog_category b', 'b.id = a.id_category');
        $this->db->like('b.category_name', $parameter);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_total_search($search = null)
    {
        $this->db->from('cms_blog_post');
        $this->db->like('title', $search, 'both');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getAllArticle()
    {
        $query = $this->db->get('cms_blog_post');
        return $query->result_array();
    }

    public function getActicleHome()
    {
        $this->db->limit(3);
        $this->db->select('a.*, b.category_name');
        $this->db->from('cms_blog_post a');
        $this->db->join('cms_blog_category b', 'b.id = a.id_category');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPost($slug)
    {
        $query = $this->db->select('a.*, b.first_name, b.last_name, b.image as profile, b.about_me')->join('cms_user b', 'b.id = a.id_creator')->get_where('cms_blog_post a', array('post_slug' => $slug));
        return $query->row_array();
    }

    function recentPostArticle()
    {
        $query = $this->db->limit(5)->get_where('cms_blog_post', array('flag' => 'lowongan'));
        return $query->result_array();
    }

    function recentTagArticle()
    {
        $query = $this->db->limit(5)->where('flag', 'lowongan')->get('cms_blog_post');
        return $query->result_array();
    }

    public function getCategory()
    {
        $this->db->select('b.category_name, count(b.category_name) as count_category');
        $this->db->from('cms_blog_post a');
        $this->db->join('cms_blog_category b', 'b.id = a.id_category');
        $this->db->where('a.flag', 'lowongan');
        $query = $this->db->get();
        return $query->result_array();
    }

    function countComment($slug)
    {
        $this->db->select('a.*, c.first_name, c.last_name, c.image');
        $this->db->from('cms_blog_comment a');
        $this->db->join('cms_blog_post b', 'b.id = a.id_post');
        $this->db->join('cms_user c', 'c.id = a.id_user');
        $where = "b.post_slug = '$slug' AND b.flag = 'lowongan'";
        $this->db->where($where);
        $query = $this->db->count_all_results();
        return $query;
    }

    function updateCountComment($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function getCommentArticle($slug)
    {
        $this->db->select('a.*, c.first_name, c.last_name, c.image');
        $this->db->from('cms_blog_comment a');
        $this->db->join('cms_blog_post b', 'b.id = a.id_post');
        $this->db->join('cms_user c', 'c.id = a.id_user');
        $this->db->where('b.post_slug', $slug);
        $query = $this->db->get();
        return $query->result_array();
    }

    function getTag()
    {
        $query = $this->db->select('*')->from('cms_blog_tag')->group_by('tag_name')->where('flag', 'lowongan')->get();
        return $query->result_array();
    }

    function getArticleTag($where)
    {
        $this->db->select('b.*, a.tag_name');
        $this->db->from('cms_blog_tag a');
        $this->db->join('cms_blog_post b', 'a.id_post = b.id');
        $this->db->where('a.tag_name', $where);
        $query = $this->db->get();
        return $query->result_array();
    }
}

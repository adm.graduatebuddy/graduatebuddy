<?php

function is_logged_in()
{
    $ci = get_instance();
    if (!$ci->session->userdata('email')) {
        redirect('cms/auth');
    }
}

function is_active_maintenance()
{
    $ci = get_instance();
    if ($ci->session->userdata('email')) {
        redirect('maintenance');
    }
}

function login_member()
{
    $ci = get_instance();
    if ($ci->session->userdata('id_student')) {
        redirect('member/home');
    }
}

function logout_member()
{
    $ci = get_instance();
    if (!$ci->session->userdata('id_student')) {
        redirect('home');
    }
}

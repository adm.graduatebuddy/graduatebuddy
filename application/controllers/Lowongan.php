<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lowongan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('Frontend_model', 'frontend');
        $this->load->model('Lowongan_model', 'lowongan');
    }

    public function page()
    {
        is_active_maintenance();
        $data = $this->siteSettings();
        $data['recent_post'] = $this->lowongan->recentPostArticle();
        $data['get_category'] = $this->lowongan->getCategory();
        $data['tag_post'] = $this->lowongan->getTag();
        $data['page'] = "single_page_menu";
        $config = array();
        $config["base_url"] = base_url() . "lowongan/page";
        $config["total_rows"] = $this->lowongan->get_total();
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link']       = '<i class="ti-angle-left"></i><i class="ti-angle-left"></i>';
        $config['last_link']        = '<i class="ti-angle-right"></i><i class="ti-angle-right"></i>';
        $config['next_link']        = '<i class="ti-angle-right"></i>';
        $config['prev_link']        = '<i class="ti-angle-left"></i>';
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $data['posts'] = $this->lowongan->fetchAllArticle($config["per_page"], $page);

        $this->load->view('theme/header', $data);
        $this->load->view('front/lowongan', $data);
        $this->load->view('theme/sidebar_lowongan', $data);
        $this->load->view('theme/footer', $data);
    }

    public function view($slug)
    {
        is_active_maintenance();
        $data = $this->siteSettings();
        $data['post_row'] = $this->lowongan->getPost($slug);
        $data['recent_post'] = $this->lowongan->recentPostArticle();
        $data['get_category'] = $this->lowongan->getCategory();
        $data['tag_post'] = $this->lowongan->getTag();
        $data['get_comment'] = $this->lowongan->getCommentArticle($slug);
        $data['count_comment'] = $this->lowongan->countComment($slug);
        $data['page'] = "single_page_menu";
        $data['name_user'] = $this->session->userdata('name_user');
        $data['email_user'] = $this->session->userdata('email');
        if ($data['post_row'] != null) {
            $this->load->view('theme/header', $data);
            $this->load->view('front/detail_lowongan', $data);
            $this->load->view('theme/sidebar_lowongan', $data);
            $this->load->view('theme/footer', $data);
        } else {
            redirect('lowongan');
        }
    }

    public function tag($param)
    {
        is_active_maintenance();
        if ($param != null) {
            $data = $this->siteSettings();
            $data['recent_post'] = $this->lowongan->recentPostArticle();
            $data['get_category'] = $this->lowongan->getCategory();
            $data['tag_post'] = $this->lowongan->getTag();
            $data['page'] = "single_page_menu";
            $parameter  = $param; //this is your passing parameter-----
            $config = array();
            $config["base_url"] = base_url() . "lowongan/tag/" . $param;
            $config["total_rows"] = $this->lowongan->get_total_tag($parameter);
            $config["per_page"] = 5;
            $config["uri_segment"] = 4;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = floor($choice);

            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

            // Membuat Style pagination untuk BootStrap v4
            $config['first_link']       = '<i class="ti-angle-left"></i><i class="ti-angle-left"></i>';
            $config['last_link']        = '<i class="ti-angle-right"></i><i class="ti-angle-right"></i>';
            $config['next_link']        = '<i class="ti-angle-right"></i>';
            $config['prev_link']        = '<i class="ti-angle-left"></i>';
            $config['full_tag_open']    = '<ul class="pagination">';
            $config['full_tag_close']   = '</ul>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $data['posts'] = $this->lowongan->fetchTagArticle($config["per_page"], $page, $parameter);

            $this->load->view('theme/header', $data);
            $this->load->view('front/lowongan', $data);
            $this->load->view('theme/sidebar_lowongan', $data);
            $this->load->view('theme/footer', $data);
        } else {
            redirect('lowongan/');
        }
    }

    public function search()
    {
        is_active_maintenance();
        $data = $this->siteSettings();
        $data['recent_post'] = $this->lowongan->recentPostArticle();
        $data['get_category'] = $this->lowongan->getCategory();
        $data['tag_post'] = $this->lowongan->getTag();
        $data['page'] = "single_page_menu";
        // get search string
        $search = ($this->input->post("search_post")) ? $this->input->post("search_post") : "NIL";
        $search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;

        $config = array();
        $config["base_url"] = base_url() . "lowongan/search/" . $search;
        $config["total_rows"] = $this->lowongan->get_total_search($search);
        $config["per_page"] = 5;
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : $search;

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link']       = '<i class="ti-angle-left"></i><i class="ti-angle-left"></i>';
        $config['last_link']        = '<i class="ti-angle-right"></i><i class="ti-angle-right"></i>';
        $config['next_link']        = '<i class="ti-angle-right"></i>';
        $config['prev_link']        = '<i class="ti-angle-left"></i>';
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $data['posts'] = $this->lowongan->fetchSearchArticle($config["per_page"], $page, $search);

        $this->load->view('theme/header', $data);
        $this->load->view('front/lowongan', $data);
        $this->load->view('theme/sidebar_lowongan', $data);
        $this->load->view('theme/footer', $data);
    }

    public function category($param)
    {
        is_active_maintenance();
        if ($param != null) {
            $data = $this->siteSettings();
            $data['recent_post'] = $this->lowongan->recentPostArticle();
            $data['get_category'] = $this->lowongan->getCategory();
            $data['tag_post'] = $this->lowongan->getTag();
            $data['page'] = "single_page_menu";
            $parameter  = $param; //this is your passing parameter-----
            $config = array();
            $config["base_url"] = base_url() . "lowongan/category/" . $param;
            $config["total_rows"] = $this->lowongan->get_total_category($parameter);
            $config["per_page"] = 1;
            $config["uri_segment"] = 4;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = floor($choice);

            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

            // Membuat Style pagination untuk BootStrap v4
            $config['first_link']       = '<i class="ti-angle-left"></i><i class="ti-angle-left"></i>';
            $config['last_link']        = '<i class="ti-angle-right"></i><i class="ti-angle-right"></i>';
            $config['next_link']        = '<i class="ti-angle-right"></i>';
            $config['prev_link']        = '<i class="ti-angle-left"></i>';
            $config['full_tag_open']    = '<ul class="pagination">';
            $config['full_tag_close']   = '</ul>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $data['posts'] = $this->lowongan->fetchCategoryArticle($config["per_page"], $page, $parameter);

            $this->load->view('theme/header', $data);
            $this->load->view('front/lowongan', $data);
            $this->load->view('theme/sidebar_lowongan', $data);
            $this->load->view('theme/footer', $data);
        } else {
            redirect('lowongan/');
        }
    }

    public function post_comment($id_post)
    {
        is_active_maintenance();

        if ($id_post != null) {
            $slug = $this->input->post('slug_comment');

            $data = [
                'id_post' => $id_post,
                'id_user' => $this->session->userdata('id'),
                'comment_post' => $this->input->post('comment_text'),
                'website' => $this->input->post('website_comment'),
                'flag' => 'lowongan'
            ];

            $this->db->insert('cms_blog_comment', $data);
            redirect('lowongan/post/' . $slug);
            $count = $this->lowongan->countComment($slug);
            $this->updateCount($slug, $count);
        } else {
            redirect('lowongan/');
        }
    }

    public function updateCount($slug, $count)
    {
        $where = array('post_slug' => $slug, 'flag' => 'lowongan');
        $data = [
            'count_comment' => $count
        ];

        $this->lowongan->updateCountComment($where, $data, 'cms_blog_post');
    }

    public function siteSettings()
    {
        $data['footer_kiri'] = $this->frontend->view_data("1");
        $data['footer_tengah'] = $this->frontend->view_data("2");
        $data['footer_kanan'] = $this->frontend->view_data("3");
        $data['footer_bawah'] = $this->frontend->view_data("4");
        $data['logo'] = $this->frontend->view_data("5");
        $data['is_login'] = $this->session->userdata('id_student');
        if ($data['is_login'] != null) {
            $data['menu_header'] = $this->frontend->view_menu('login');
        } else {
            $data['menu_header'] = $this->frontend->view_menu('not_login');
        }
        $data['submenu_header'] = $this->frontend->view_submenu();
        return $data;
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->config('email');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->model('frontend_model', 'frontend');
        $this->load->model('blog_model', 'blog');
        $this->load->model('pdf_model', 'pdf');
        $this->load->model('member_model', 'member');
        $this->load->helper('date');
        logout_member();
    }

    public function index()
    {
        is_active_maintenance();
        $data = $this->siteSettings();
        $data['page'] = "single_page_menu";
        $data['blog'] = $this->blog->getActicleHome();
        $data['about'] = $this->frontend->view_data("6");
        $data['paket_name'] = $this->frontend->get_data_profil($this->session->userdata('id_student'));
        $data['data_profil'] = $this->frontend->get_profil($this->session->userdata('id_student'));
        $name = explode(" ", $this->session->userdata('full_name_student'));
        $data['welcome_name'] = $name[0] . " " . $name[1];
        $user = $this->frontend->get_profil_member($this->session->userdata('id_student'));
        if ($user->paket != 0 && $user->status_paket == 2) {
            $data['file_pdf_psikotest'] = $this->pdf->get_pdf_psikotest($user->paket);
            $data['file_pdf_wawancara'] = $this->pdf->get_pdf_wawancara($user->paket);
            $this->load->view('theme/header', $data);
            $this->load->view('member/home');
            $this->load->view('theme/footer', $data);
            $this->load->view('theme/script_home_login');
        } else {
            redirect('member/profil');
        }
    }

    public function get_data_pdf($id)
    {
        is_logged_in();
        $data = $this->pdf->get_pdf_by_id($id);
        echo json_encode($data);
    }

    public function paket()
    {
        is_active_maintenance();
        $data = $this->siteSettings();
        $data['page'] = "home_menu";
        $data['paket'] = $this->frontend->view_data("8");
        $data['data_profil'] = $this->frontend->get_profil($this->session->userdata('id_student'));
        $data['paket_pilih'] = $this->frontend->view_paket();
        $data['user_data'] = $this->frontend->get_profil_member($this->session->userdata('id_student'));
        $this->load->view('theme/header', $data);
        $this->load->view('member/paket', $data);
        $this->load->view('theme/footer', $data);
    }

    public function verify_paket()
    {
        is_active_maintenance();
        $this->form_validation->set_rules('full_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('no_hp', 'Phone', 'required|trim');
        $this->form_validation->set_rules('umur', 'Umur', 'required|trim');
        $this->form_validation->set_rules('tempat_tgl_lahir', 'Tempat, Tanggal Lahir', 'required|trim');
        $this->form_validation->set_rules('universitas', 'Universitas', 'required|trim');
        $this->form_validation->set_rules('lulus', 'Lulus', 'required|trim');
        $this->form_validation->set_rules('paket', 'Paket', 'required|trim');
        $where = ['id' => $this->session->userdata('id_student')];

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImage($this->input->post('old_logo'));
            $logo = $this->_uploadImage("member_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'name' => $this->input->post('full_name'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('no_hp'),
            'photo' => $logo,
            'umur' => $this->input->post('umur'),
            'tempat_tgl_lahir' => $this->input->post('tempat_tgl_lahir'),
            'universitas_name' => $this->input->post('universitas'),
            'graduate_month_year' => $this->input->post('lulus'),
            'paket' => $this->input->post('paket'),
            'status_paket' => 1
        ];

        if ($this->form_validation->run() == true) {
            $this->frontend->verify_paket($where, $data, 'cms_user_student');
            $this->session->set_flashdata('message_edit_profile', '<div class="alert alert-success" role="alert">Perubahan data pilih paket dan verifikasi profile sukses! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('member/profil');
        } else {
            redirect('member/profil');
        }
    }

    public function upgrade_paket()
    {
        is_active_maintenance();
        $this->form_validation->set_rules('full_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('phone', 'Phone', 'required|trim');
        $this->form_validation->set_rules('paket', 'Paket', 'required|trim');

        $where = [
            'name' => $this->input->post('full_name'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone')
        ];

        $data = [
            'paket' => $this->input->post('paket'),
            'status_paket' => 0,
        ];

        if ($this->form_validation->run() == true) {
            $this->send_cara_bayar($this->input->post('paket'), $this->input->post('email'), $this->input->post('full_name'));
            $this->member->upgrade_paket($where, $data, 'cms_user_student');
            $this->session->set_flashdata('message_upgrade_paket', '<div class="alert alert-success" role="alert">Upgrade paket anda sukses! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('member/paket');
        } else {
            redirect('member/paket');
        }
    }

    public function send_cara_bayar($id_paket, $email, $name)
    {
        $config_email = $this->member->config_bayar_upgrade_paket();
        $get_paket = $this->frontend->get_data_paket($id_paket);
        $before_replace = array('$nama', '$paket_name', '$paket_price');
        $after_replace = array($name, $get_paket->nama_paket, "Rp. " . number_format($get_paket->harga_paket, 2, ',', '.'));
        $from = $this->config->item('smtp_user');
        $to = $email;
        $subject = $config_email->judul;
        $message = str_replace($before_replace, $after_replace, $config_email->content);

        $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            echo 'Your Email has successfully been sent.';
        } else {
            show_error($this->email->print_debugger());
        }
    }

    public function profil($id_peket = null)
    {
        is_active_maintenance();
        $data = $this->siteSettings();
        $data['page'] = "home_menu";
        $data['paket'] = $this->frontend->view_data("8");
        $data['paket_select'] = $id_peket;
        $data['paket_pilih'] = $this->frontend->view_paket();
        $data['paket_name'] = $this->frontend->get_data_profil($this->session->userdata('id_student'));
        $data['data_profil'] = $this->frontend->get_profil($this->session->userdata('id_student'));
        $data['cara_bayar'] = $this->frontend->get_cara_bayar();
        $this->load->view('theme/header', $data);
        $this->load->view('member/profil', $data);
        $this->load->view('theme/footer', $data);
    }

    public function edit_profil()
    {
        is_active_maintenance();
        $this->form_validation->set_rules('full_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('umur', 'Umur', 'required|trim');
        $this->form_validation->set_rules('domisili', 'Kota Domisili', 'required|trim');
        $this->form_validation->set_rules('universitas', 'Universitas', 'required|trim');
        $where = ['id' => $this->session->userdata('id_student')];

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImage($this->input->post('old_logo'));
            $logo = $this->_uploadImage("member_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'name' => $this->input->post('full_name'),
            'email' => $this->input->post('email'),
            'photo' => $logo,
            'umur' => $this->input->post('umur'),
            'kota_domisili' => $this->input->post('domisili'),
            'universitas_name' => $this->input->post('universitas')
        ];

        if ($this->form_validation->run() == true) {
            $this->frontend->verify_paket($where, $data, 'cms_user_student');
            $this->session->set_flashdata('message_edit_profile', '<div class="alert alert-success" role="alert">Perubahan data profil sukses! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('member/profil');
        } else {
            redirect('member/profil');
        }
    }

    public function siteSettings()
    {
        $data['is_login'] = $this->session->userdata('id_student');
        $data['footer_kiri'] = $this->frontend->view_data("1");
        $data['footer_tengah'] = $this->frontend->view_data("2");
        $data['footer_kanan'] = $this->frontend->view_data("3");
        $data['footer_bawah'] = $this->frontend->view_data("4");
        $data['logo'] = $this->frontend->view_data("5");
        $data['menu_header'] = $this->frontend->view_menu("login");
        $data['submenu_header'] = $this->frontend->view_submenu();
        return $data;
    }

    private function _uploadImage($param)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('His');

        $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/profile/";
        $config['file_name'] = "pp_" . $param . $now;
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload("logo")) {
            $error = $this->upload->display_errors();
            // menampilkan pesan error
            print_r($error);
        } else {
            $upload = array('upload_data' => $this->upload->data());
            return $upload['upload_data']['file_name'];
        }

        return "default.jpg";
    }

    private function _deleteImage($file)
    {
        unlink($_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/profile/" . $file);
    }

    public function logout()
    {
        $this->session->unset_userdata('id_student');
        $this->session->unset_userdata('full_name_student');
        $this->session->unset_userdata('email_student');
        $this->session->unset_userdata('phone_student');
        $this->session->unset_userdata('status_paket_student');
        $this->session->unset_userdata('paket_student');
        $this->session->set_flashdata('message', '<div class="alert alert-success" id="alert-success" role="alert" style="width: 350px; position:absolute; right:20px;">Anda telah logout!
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>');
        redirect('home');
    }

    public function download_workbook()
    {
        is_active_maintenance();
        force_download('assets/uploads/template/free_cv_template.pdf', NULL);
    }
}
